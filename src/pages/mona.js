import * as React from "react"
import { Link } from "gatsby"

import Layout from '../components/layout.js'
import * as indexPageStyles from "../styles/index.module.scss"
import * as linkStyles from "../styles/links.module.scss"
import monaImg from "../images/mona.jpg"

const MonaPage = () => {
  return (
	<Layout title="Mona">
		<div className={indexPageStyles.menu}>
			<div className={linkStyles.menu}>
			<h1 className={linkStyles.menuText}>Mona</h1>
            <img className={linkStyles.headerImg} src={monaImg} alt="release artwork"/>
			<div className={linkStyles.menuLinks}>
				<h2 className={linkStyles.menuText}><a href="https://open.spotify.com/album/40IvEmVB6h7wG48C4KSnBf?si=KMATDtlMRomPXG1moFr6gw">Spotify</a></h2>
				<h2 className={linkStyles.menuText}><a href="https://music.apple.com/gb/album/mona/1603899248?i=1603899252">Apple Music</a></h2>
	  			<h2 className={linkStyles.menuText}><Link to="https://www.youtube.com/watch?v=A2iEGGF_Fsg">Music Video</Link></h2>
				<h2 className={linkStyles.menuText}><Link to="/">Website</Link></h2>
			</div>
			</div>
		</div>
	</Layout>
  )
}

export default MonaPage
