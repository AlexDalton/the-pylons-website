import * as React from "react"
import { Link } from "gatsby"

import Layout from '../components/layout.js'
import * as indexPageStyles from "../styles/index.module.scss"
import * as linkStyles from "../styles/links.module.scss"
import rustyImg from "../images/rusty.jpg"

const RustyPage = () => {
  return (
	<Layout title="Rusty">
		<div className={indexPageStyles.menu}>
			<div className={linkStyles.menu}>
			<h1 className={linkStyles.menuText}>Rusty</h1>
            <img className={linkStyles.headerImg} src={rustyImg} alt="release artwork"/>
			<div className={linkStyles.menuLinks}>
				<h2 className={linkStyles.menuText}><a href="https://open.spotify.com/album/4GQEm0ClW9xlJCP3bMcmS6?si=nR48N1R0QIGjPJf2bcVXOQ">Spotify</a></h2>
				<h2 className={linkStyles.menuText}><a href="https://music.apple.com/gb/album/rusty/1625141443?i=1625141631">Apple Music</a></h2>
	  			<h2 className={linkStyles.menuText}><Link to="https://www.youtube.com/watch?v=jCpSNMzG8Wc">Music Video</Link></h2>
				<h2 className={linkStyles.menuText}><Link to="/">Website</Link></h2>
			</div>
			</div>
		</div>
	</Layout>
  )
}

export default RustyPage
