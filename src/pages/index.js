import * as React from "react"
import { Helmet } from 'react-helmet'

import Layout from '../components/layout.js'
import * as indexPageStyles from "../styles/index.module.scss"
import Menu from "../components/menu.js"

const IndexPage = () => {
  return (
	<Layout title="The Pylons">
		<Helmet>
			<meta name="facebook-domain-verification" content="hkusbl3z9pqzmgb70parpimtvtjeoq" />
		</Helmet>
		<div className={indexPageStyles.menu}>
			<Menu />
		</div>
	</Layout>
  )
}

export default IndexPage
