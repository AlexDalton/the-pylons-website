import * as React from "react"

import Layout from '../components/layout.js'
import Menu from "../components/menu.js"
import Tourbox from "../components/tourbox.js"

import * as tourPageStyles from "../styles/tour.module.scss"

const TourPage = () => {
  return (
	<Layout title="Tour">
		<div className={tourPageStyles.menu}>
			<Menu />
	  	</div>
		<div className={tourPageStyles.tourbox}>
	  		<Tourbox />
		</div>
	</Layout>
  )
}

export default TourPage
