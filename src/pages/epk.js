import * as React from "react"

import Layout from '../components/layout.js'
import Menu from "../components/menu.js"

import * as musicPageStyles from "../styles/music.module.scss"
import * as epkPageStyles from "../styles/epk.module.scss"
import pressShot from "../images/press-shot.jpg"

const MusicPage = () => {
  return (
	<Layout title="EPK">
		<div className={musicPageStyles.menu}>
			<Menu />
		</div>
	  	<div className={epkPageStyles.outercontent}>
	  	<div className={epkPageStyles.content}>
	  	<img className={epkPageStyles.pressshot} src={pressShot} alt="Press Shot"/>
	  	<p>They were once a quadruple of escapees from the beauty of Lincolnshire, who made their
way to the big smoke and found refuge in North West London. They were joined by a solo
waif from the depths of Essex, who, lacking the cursory bronzed look and false eyelashes,
decided to head in the same general direction. At this point, the four became five and The
Pylons were complete and ready to unleash their creative juices on an unsuspecting
worldwide audience.</p>
	  <hr />

	  <h2> Current Release: Rusty</h2>
	  <iframe className={epkPageStyles.spotify} src="https://open.spotify.com/embed/track/2KwmheuYwyBJuLLqiZo5rP?utm_source=generator" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
	  <p>Rusty has been, since its inception, a creature that has moved quickly, taking very little time to be fully formed as an angry, contemplative song about finding it really difficult to care anymore. Within moments of its completion, it was never in doubt that it would jump to the front of the long queue of songs</p>
	  <iframe className={epkPageStyles.video} src="https://www.youtube.com/embed/jCpSNMzG8Wc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	  <p>Each track on the EP is accompanied by a music video - self produced by the band and View Shift Productions.</p>

<p>The videos tell the story of a man inspired by the spirit of the old theatre he works to live out fantasies of fame, fantasies which begin to get away from him. He descended through these dreams pursued by an enigmatic figure representing a dark mirror version of the audience, shining a twisted version of the spotlight he craves.</p>

<p>Rusty is the climax of this descent. The carefully constructed barriers in his dreams collapse and bleed into each other. Even his sense of self isn't safe as he splits into fragments and morphs into the other members of the band.</p>


	  <hr />
	  	<h2><span className={epkPageStyles.normal}>Previous Release:</span> Mona</h2>
	  <iframe className={epkPageStyles.spotify} src="https://open.spotify.com/embed/track/3zNllcyezP9emjH8Cd4pq3?utm_source=generator&theme=0" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
	  <p>Self-produced at RYP Recordings and mastered by Andy ‘Hippy Baldwin’ at Metropolis
Studios, ‘MONA’ is set to spring their next chapter to life with intricate guitar licks, smooth
vocals and a nod to 00s indie-era. It’s noted that there’s nothing these boys can’t do; the
music video for this sound offering was created through boredom and sheer creativity. They
wrote and filmed a TV show in their living room which was based on a 70s talk show.
'Vincent Relish's Days of Future Past' saw long time collaborator and sound engineer Barny
host a show which included a house band (The Pylons themselves), an interview with
special guests, a cookery segment, sports news, street reporter interviewing the public
(played by the band), an ad break and more. They then started putting our time to good use
and actually working towards a release...and the ‘MONA’ video was born.</p>
	  <iframe className={epkPageStyles.video} src="https://www.youtube.com/embed/A2iEGGF_Fsg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<p>Speaking on the track, the band said: “MONA was hauled out from between the sofa
cushions of our demos folder, an old favourite that took us a long time to bring ourselves to
finish and release, for fear of it being gone. MONA is the self-perpetuating narrative that runs
through one's mind as you see what you desire at a distance further than can be reached.
This track has been strained and filtered through our collective struggles of the past few
years and is ultimately a reflection of who we were before through the lense of who we are
now."</p>

	<hr />
	  	<h2>Upcoming EP: Thanks For Coming</h2>
	<iframe className={epkPageStyles.soundcloud} scrolling="no" frameborder="0" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1191400882%3Fsecret_token%3Ds-35SsQ7s1t0H&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div className={epkPageStyles.innersoundcloud}><a href="https://soundcloud.com/thepylonsband" title="The Pylons" target="_blank" className={epkPageStyles.innersoundcloudlink}>The Pylons</a> · <a href="https://soundcloud.com/thepylonsband/sets/the-pylons-2021-ep-demos" title="The Pylons 2022 EP Demos" target="_blank" className={epkPageStyles.innersoundcloudlink}>The Pylons 2022 EP Demos</a></div>

	<hr />
	  	<h2>Previous Releases</h2>
	<iframe frameborder="0" className={epkPageStyles.bigspotify} src="https://open.spotify.com/embed/playlist/3NNHwYu2I9vcu0tdgzVv8O?utm_source=generator&theme=0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

	  	</div>
	</div>
	</Layout>
  )
}

export default MusicPage
