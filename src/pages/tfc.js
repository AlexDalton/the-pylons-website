import * as React from "react"
import { Link } from "gatsby"

import Layout from '../components/layout.js'
import * as indexPageStyles from "../styles/index.module.scss"
import * as linkStyles from "../styles/links.module.scss"
import tfcImg from "../images/tfc.jpg"

const tfcPage = () => {
  return (
	<Layout title="Thanks for Coming">
		<div className={indexPageStyles.menu}>
			<div className={linkStyles.menu}>
			<h1 className={linkStyles.menuText}>Thanks for Coming</h1>
            <img className={linkStyles.headerImg} src={tfcImg} alt="release artwork"/>
			<div className={linkStyles.menuLinks}>
				<h2 className={linkStyles.menuText}><a href="https://open.spotify.com/album/0TMsFeLLEhUVoMgVSvzC10?si=45e0e9294a2c41a6">Spotify</a></h2>
				<h2 className={linkStyles.menuText}><a href="https://music.apple.com/gb/album/thanks-for-coming-ep/1646744301">Apple Music</a></h2>
				<h2 className={linkStyles.menuText}><a href="https://www.youtube.com/watch?v=FvRuTBrSlM4">Video</a></h2>
				<h2 className={linkStyles.menuText}><Link to="/">Website</Link></h2>
			</div>
			</div>
		</div>
	</Layout>
  )
}

export default tfcPage
