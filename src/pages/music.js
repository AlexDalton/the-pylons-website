import * as React from "react"

import Layout from '../components/layout.js'
import Menu from "../components/menu.js"
import Tune from "../components/tune.js"

import * as musicPageStyles from "../styles/music.module.scss"

import creaturesImg from "../images/creatures.jpg"
import floatImg from "../images/float.jpg"
import monoImg from "../images/monochrome.jpg"
import feelImg from "../images/feelgood.jpg"
import leavingImg from "../images/leaving.jpg"
import grumbleImg from "../images/grumble.jpg"
import chaseImg from "../images/thechase.jpeg"
import monaImg from "../images/mona.jpg"
import rustyImg from "../images/rusty.jpg"
import tfcImg from "../images/tfc.jpg"

const MusicPage = () => {
  return (
	<Layout title="Music">
		<div className={musicPageStyles.menu}>
			<Menu />
		</div>
		<Tune imgSrc={tfcImg} name="Thanks for Coming" link="/tfc"/>
		<Tune imgSrc={rustyImg} name="Rusty" link="/rusty"/>
		<Tune imgSrc={monaImg} name="Mona" link="/mona"/>
		<Tune imgSrc={chaseImg} name="The Chase" link="https://lnkfi.re/yh0Gxzz1"/>
		<Tune imgSrc={grumbleImg} name="Grumble" link="https://lnkfi.re/Sydsqcm7"/>
		<Tune imgSrc={leavingImg} name="Leaving" link="https://awal.ffm.to/leaving"/>
		<Tune imgSrc={feelImg} name="Feel Good" link="https://lnkfi.re/WQFatOsM"/>
		<Tune imgSrc={monoImg} name="Monochrome" link="https://lnkfi.re/Jr8OgTKx"/>
		<Tune imgSrc={floatImg} name="Float Away" link="https://lnkfi.re/LSrJGVg5"/>
		<Tune imgSrc={creaturesImg} name="Creatures" link="https://lnkfi.re/gvOP9fxJ"/>
	</Layout>
  )
}

export default MusicPage
