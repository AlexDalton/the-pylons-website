import React from "react"

import * as tourboxStyles from "../styles/tourbox.module.scss"

class Tourbox extends React.Component {
    componentDidMount() {
        const script = document.createElement('script');
        script.src = "//widget.songkick.com/401633/widget.js";
        script.async = true;
        document.getElementById("myContainer").appendChild(script);
    }

    render () {
        return (
            <div className={tourboxStyles.container} id="myContainer">
            <a href="https://www.songkick.com/artists/401633" class="songkick-widget" data-theme="dark" data-other-artists="on" data-detect-style="true" data-background-color="transparent">Pylons Tour Dates</a>
            </div>
        );
    }
}

export default Tourbox
