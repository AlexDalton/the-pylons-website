import * as React from "react"
import CookieConsent, { Cookies } from "react-cookie-consent"
import { useLocation } from "@reach/router"
import { initializeAndTrack } from 'gatsby-plugin-gdpr-cookies'
import { Helmet } from 'react-helmet'

import "../styles/index.scss"
import * as layoutStyles from "../styles/layout.module.scss"

class Layout extends React.Component {
	render() {
		return (
			<div>
			<Helmet title={this.props.title} />
			<CookieConsent
				location="bottom"
				buttonText="Accept"
				declineButtonText="Decline"
				enableDeclineButton
				cookieName="PylonsWebsiteAnalytics"
				style={{ background: "white", color: "black", fontFamily: "Arial" }}
				onAccept={() => {
					Cookies.set("gatsby-gdpr-google-analytics", "true");
					Cookies.set("gatsby-gdpr-facebook-pixel", "true");
					/* const location = useLocation()
					initializeAndTrack(location); */
				}}>
				This website uses cookies to prove to our parents this band stuff hasn't been a waste of time.
			</CookieConsent>
			<div className={layoutStyles.videoBackground}>
				<div className={layoutStyles.videoForeground}>
					<iframe title="background-video" className={layoutStyles.youtubeEmbed} src="https://www.youtube.com/embed/TKS-64RsPZA?start=14&controls=0&showinfo=0&rel=0&autoplay=1&loop=1&mute=1&playlist=TKS-64RsPZA" frameBorder="0" allowFullScreen></iframe>
				</div>
			</div>
			<div className={layoutStyles.foreground}>
			</div>
			<div className={layoutStyles.content}>
				{this.props.children}
			</div>
			</div>
		)
	}
}

export default Layout
