import * as React from "react"
import { MediaQuery } from "react-responsive"

import * as tuneStyles from "../styles/tune.module.scss"

class Tune extends React.Component {
    render() {
        return (
            <div>
            <div className={tuneStyles.desktopWidget}>
            <a href={this.props.link} rel="noreferrer" target="_blank">
                <img className={tuneStyles.image} src={this.props.imgSrc} alt="release artwork"/>
                <p className={tuneStyles.description}>{this.props.name}</p>
            </a>
            </div>
            <div className={tuneStyles.mobileWidget}>
                <a href={this.props.link} rel="noreferrer" target="_blank">
                <img className={tuneStyles.image} src={this.props.imgSrc} alt="release artwork"/>
                </a>
            </div>
            </div>
        )
    }
}

export default Tune
