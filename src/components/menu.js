import * as React from "react"
import { Link } from "gatsby"

import "../styles/index.scss"
import * as menuStyles from "../styles/menu.module.scss"

class Menu extends React.Component {
	render () {
		return (
			<div className={menuStyles.menu}>
			<h1 className={menuStyles.menuText}><Link to="/">Pylons</Link></h1>
			<div className={menuStyles.menuLinks}>
			<h2 className={menuStyles.menuText}><Link to="/music">Music</Link></h2>
			<h2 className={menuStyles.menuText}><Link to="/tour">Tour</Link></h2>
			<h2 className={menuStyles.menuText}><a href="https://thepylons.us17.list-manage.com/subscribe?u=f0ded580b0ad897aeda36539f&id=fda9078f30">Newsletter</a></h2>
			</div>
			</div>
		)
	}
}

export default Menu
