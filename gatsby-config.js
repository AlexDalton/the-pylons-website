module.exports = {
  siteMetadata: {
    siteUrl: "https://www.pylonsband.com",
    title: "Pylons",
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-image",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
	{
      resolve: `gatsby-plugin-gdpr-cookies`,
      options: {
        googleAnalytics: {
          trackingId: 'G-W28L96H5RT',
          anonymize: true
        },
        facebookPixel: {
          pixelId: '240081561624437'
        },
      },
    },
	{
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "Pylons",
        short_name: "Pylons",
        start_url: "/",
        display: "browser",
        icon: "./src/images/icon.png",
      },
    },	
  ],
};
